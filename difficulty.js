'use strict'
        
        //Generating Random colours 
        function generateRandomColor(difficulty) {
            let min = 0;
            let max = 255;
            let color = [];
            const diff = [80, 40, 10][difficulty - 1];
            let range = Math.floor(Math.random() * (255 - 0 + 1) + 0);
    
            // If the difficulty level is greater than or equal to 1, calculate the minimum allowed difference
            if (difficulty >= 1) {
                // Calculate the minimum and maximum allowed values 
                // make sure the number is in the range of rbg
                min = Math.max(0, range - diff);
                max = Math.min(255, range + diff);
            }

            for(let i = 0; i < 3; i++){
                color[i] = Math.floor(Math.random() * (max - min + 1) + min);
            }

            return `rgb(${color[0]},${color[1]},${color[2]})`;
        }
        