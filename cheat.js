'use strict'

let cheatMode = false;

// Function to toggle cheat mode
function toggleCheatMode() {
    const squares = document.querySelectorAll('.game-rectangle');

    cheatMode = !cheatMode;
    if (cheatMode) {
        document.body.classList.add('cheat');
        squares.forEach(square => {
            const rgb = getRGB(square);
            const { colorName, textColor } = closestColorName(rgb);
            square.innerHTML = `${rgb} <br> ${colorName}`;
            square.style.color = textColor;
        });
    } else {
        document.body.classList.remove('cheat');
        squares.forEach(square => {
            square.textContent = '';
            square.style.color = 'black';
        });
    }
}

// Function to get RGB values of a square
function getRGB(square) {
    const computedStyle = getComputedStyle(square);
    return computedStyle.backgroundColor;
}

// Function to get the color name
function closestColorName(rgb) {
    const [r, g, b] = rgb
        .substring(4, rgb.length - 1)
        .split(', ')
        .map(Number);
    const fontColor = (r * 299 + g * 587 + b * 114) / 1000;
    let textColor = 'white'; // Default text color
    let colorName = 'Unknown';

    if (fontColor < 128) {
        textColor = 'white'; // Use white text for dark backgrounds
    } else {
        textColor = 'black'; // Use black text for light backgrounds
    }

    if (r > g && r > b) {
        colorName = 'Red';
    } else if (g > r && g > b) {
        colorName = 'Green';
    } else {
        colorName = 'Blue';
    }

    return { colorName, textColor };
}

// Event listener for Shift+C key press
window.addEventListener('keydown', (event) => {
    if (event.shiftKey && event.key === 'C') {
        toggleCheatMode();
    }
});
