'use strict';

document.addEventListener("DOMContentLoaded", function () {
    
    const playerNameInput = document.getElementById('player-name');
    const boardSizeInput = document.getElementById('board-size');
    const colorChoiceInput = document.getElementById('color-choice');
    const difficultyInput = document.getElementById('difficulty-input');
    const startButton = document.getElementById("start-button");
    const submitButton = document.getElementById("submit-button");
    
   
    // const chosenColor = colorChoiceInput.value;
    const message = document.getElementById("game-board-Message");

    let playerNameValue;
    let boardSizeValue;
    let colorChoiceValue;
    let difficultyValue;

    function checkFields() {
        playerNameValue = playerNameInput.value.trim();
        boardSizeValue = parseInt(boardSizeInput.value, 10);
        colorChoiceValue = colorChoiceInput.value;
        difficultyValue = difficultyInput.value;

        const allFieldsHaveValues = playerNameValue !== '' && !(boardSizeValue > 7 & boardSizeValue > 3) && colorChoiceValue !== '';

        startButton.disabled = !allFieldsHaveValues;

    }

    playerNameInput.addEventListener('input', checkFields);
    boardSizeInput.addEventListener('input', checkFields);
    colorChoiceInput.addEventListener('input', checkFields);
    difficultyInput.addEventListener('input', checkFields);

    let numTarget = 0;
    // Listen for the click on the "Start Game!" button
    startButton.addEventListener("click", function () {
        const selectColorMessage = document.getElementById("select-color");
        const expectedTarget = document.getElementById("expected-target");
        

        // Get the game board container
        const gameBoardContainer = document.getElementById("game-board-container");

        // Get the board size from the input element
        const boardSizeInputElement = document.getElementById("board-size");
        const boardSize = parseInt(boardSizeInputElement.value, 10);

        // Ensure boardSize stays within the range of 3 to 7
        const n = Math.max(3, Math.min(7, boardSize));

        // Remove any existing rectangles
        gameBoardContainer.innerHTML = "";

        // Create an n x n grid of rectangles
        for (let i = 0; i < n; i++) {
            const row = document.createElement("div");
            row.classList.add("game-row");
            for (let j = 0; j < n; j++) {
                const rectangle = document.createElement("div");
                rectangle.classList.add("game-rectangle");
                rectangle.style.backgroundColor = generateRandomColor(difficultyValue);
                if(closestColorName(rectangle.style.backgroundColor).colorName.toLocaleLowerCase() == colorChoiceValue){
                    numTarget++;
                }
                row.appendChild(rectangle);
            }
            gameBoardContainer.appendChild(row);
        }
        message.hidden = false;
        selectColorMessage.innerHTML = colorChoiceValue;
        expectedTarget.innerHTML = numTarget;
        submitButton.style.display = "block";
    });


    let count = 0;
    const selectedTarget = document.getElementById("selected-target");
     // Add a click event listener to the game board container for square selection
     document.getElementById("game-board-container").addEventListener("click", function (event) {
        const clickedSquare = event.target;

        // Check if the clicked element is a game rectangle (square)
        if (clickedSquare.classList.contains("game-rectangle")) {
            // Toggle the "selected" class
            clickedSquare.classList.toggle("selected");
            console.log();
            
            if(clickedSquare.classList.contains("selected")){
                count++;    
            } else if(!clickedSquare.classList.contains("selected")){
                count--;
            }
        }

        selectedTarget.innerHTML = count;
    });


    submitButton.addEventListener("click", function () {
        const gameBoardContainer = document.getElementById("game-board-container");
        let correctTiles = 0;
        let numSelected = 0;
        gameBoardContainer.childNodes.forEach(row => {
            row.childNodes.forEach(rectangle=>{
                let isMatchColor = closestColorName(rectangle.style.backgroundColor).colorName.toLowerCase() == colorChoiceValue;
                if(rectangle.classList.contains("selected")) {
                    if(isMatchColor)
                        correctTiles++;
                 numSelected++;
                }
            })
        });

    
        const score = getScore(correctTiles, numSelected, boardSizeValue, difficultyValue);
        saveHighScore(playerNameValue, score);
        renderScoreBoard();    

        window.alert(`Your score is : ${score}! \nYou got ${calcPercentage(correctTiles, numTarget)}% right!`);
        reset();
    });

    function reset(){
        const gameBoardContainer = document.getElementById("game-board-container");
        const valueRange = document.getElementById("rangeValue");
        
        startButton.disabled = true;
        selectedTarget.innerHTML = 0; 
        count = 0; 
        numTarget = 0; 


        playerNameInput.value = "";
        boardSizeInput.value = ""; 
        colorChoiceInput.value = "";
        valueRange.innerHTML = 0;
        gameBoardContainer.innerHTML = "";
        message.hidden = true
        submitButton.style.display = "none";

        difficultyInput.value = 0;
        
    }
    
    
});