# Color Guessing Game

The project is a color guessing game that allows the user to enter their name and choose the size of the board they want to play with (ranging from a 3x3 to a 7x7 board). Afterward, the user selects the color they want to guess. It's important to note that all the colors are not purely red, green, or blue; they have variations based on their RGB values, which makes them close to those three colors. The user also has the option to change the difficulty level, ranging from 0 (easiest) to 3 (hardest, with more points). If the user doesn't modify the difficulty, it remains at 0 by default.

Furthermore, the user can enable a feature called "cheat mode" by pressing Shift+C, which allows them to see the color name of each rectangle. Once the user completes the name, board size, and selected color, they can start the game. The user receives a hint regarding how many similar colors are on the board and how many they've chosen.

When the user is finished, they can click the "Submit" button to submit their guess and view their score on the scoreboard. If desired, they can retry the game by completing the form again.

The project consists of five JavaScript files: board.js, button.js, cheat.js, difficulty.js, and highscore.js. There are also two CSS files: styles.css and mobile.css.

## Author Info
- Name: Sina Saniei
- Course: 420 Linux
- Teacher: Adam

## Assignment
In this assignment, we are required to containerize a JavaScript application from the 320 course in 2023. We will create a Dockerfile, build and test the Docker image, and then deploy it using Netlify and Azure containers.

## Application
This application is a color guessing game that allows users to select the size of the game board, choose a color to guess, and play the game by identifying colors on the board. Users can change difficulty levels and enable cheat mode to see color names.

## Instructions
    - **Building the Dockerfile:**
        1. Navigate to the project directory containing the Dockerfile.
        2. Run the following command to build the Docker image:
           ```sh
           docker build -t color-guessing-game .
           ```
        3. Ensure that the image is built successfully by running:
           ```sh
           docker images
           ```

    - **Running the Docker Container:**
        1. Run the Docker container with the following command:
           ```sh
           docker run -d -p 80:80 --name color-guessing-game-container color-guessing-game
           ```
        2. Access the application in your web browser at `http://localhost`.

    - **Deploying to Docker Hub:**
        1. Tag the image for Docker Hub:
           ```sh
           docker tag color-guessing-game your-dockerhub-username/color-guessing-game
           ```
        2. Push the image to Docker Hub:
           ```sh
           docker push your-dockerhub-username/color-guessing-game
           ```
        3. Run the container from Docker Hub on another machine using:
           ```sh
           docker run -d -p 80:80 your-dockerhub-username/color-guessing-game
           ```

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sina5883005/color-guessing-game.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/sina5883005/color-guessing-game/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
