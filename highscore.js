'use strict'
document.addEventListener("DOMContentLoaded", function () {
    const highScoresHeader = document.getElementById("high-score-header");
    
    let isDescendingOrder = true; // default descending
    console.log(document.getElementById("high-score-header"));
    highScoresHeader.addEventListener("click", function() {
        isDescendingOrder = !isDescendingOrder;
        // get the scores from the localStorage
        const existingHighScores = JSON.parse(localStorage.getItem('highScores')) || [];
        if (isDescendingOrder) {
            // Sort high scores in descending order
           existingHighScores.sort((a, b) => b.score - a.score);
        } else {
            // Sort high scores in ascending order
           existingHighScores.sort((a, b) => a.score - b.score);
        };

        // restore the score in localStorage
        localStorage.setItem('highScores', JSON.stringify(existingHighScores));
        renderScoreBoard(); // re populate the high School Table 
    });

   
});

function getScore(numCorrect, numSelected, boardSize, difficulty) {
    const percent = ( 2 * numCorrect - numSelected ) /(boardSize * boardSize);
    return Math.floor(percent * 100 * boardSize * (difficulty + 1));
}


 // Save the Score and name to the LocalStorage
 function saveHighScore(name, score){
    const userScore = {
        name : name,
        score : score,};
    const existingHighScores = JSON.parse(localStorage.getItem('highScores')) || [];
    existingHighScores.push(userScore);
    existingHighScores.sort((a, b) => b.score - a.score); // Sort scores in descending order
    localStorage.setItem('highScores', JSON.stringify(existingHighScores));
}

// clear the localScore
function clearHightScore(){
    localStorage.clear();
}

 
// populate the ScoreBoard with the user name and scores
function renderScoreBoard(){
    const tableBody = document.getElementById("high-score-table-body");
    tableBody.innerHTML = "";
    const existingHighScores = JSON.parse(localStorage.getItem('highScores')) || [];
    // n = 10 if the existingScores is more that 10 if not n = the existingScore itself 
    const n = existingHighScores.length >= 5 ? 5 : existingHighScores.length;
    for (let i = 0; i < n; i++) {
        tableBody.innerHTML += "<tr><td>"+existingHighScores[i].name +"</td><td>"+existingHighScores[i].score +"</td><tr>";
    }
}

function calcPercentage (numCorrect, numTarget){
    return Math.floor(numCorrect * 100 / numTarget);
}
