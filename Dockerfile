# dockerfile for simple app deployment on an apache container
# 2024-5-23
# BUILD   docker build -t color-guessing-game_img .
# RUN     docker run -d -p 5020:80  -t color-guessing-game_img
# USE     http://localhost:5020


FROM httpd:2.4
LABEL maintainer="Sina Saniei" email="alisina.zahabsaniei@dawsoncollege.qc.ca" modified="2024-05-23"


# DocumentRoot for httpd becomes cwd
WORKDIR /usr/local/apache2/htdocs

# install app from host to container
COPY . ./

# expose the website port on the container
EXPOSE 80
