'use strict'


document.addEventListener("DOMContentLoaded", function () {

    //Start button background color
    const colorChoice = document.getElementById("color-choice");
    const startButton = document.getElementById("start-button");

    colorChoice.addEventListener("change", function () {
        const selectedColor = colorChoice.value;
        startButton.style.backgroundColor = selectedColor;
    });

    //Clear Button to clear score board 
    const clearButton = document.getElementById("clear-button");
    const tableBody = document.getElementById("high-score-table-body");

    clearButton.addEventListener("click", function () {
        // Clear the table body by setting its innerHTML to an empty string
        tableBody.innerHTML = "";
        clearHightScore();
    });
    
    renderScoreBoard();
});